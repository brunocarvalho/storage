Storage
=======

This is an database toolkit for php, provide an expressive query builder. Currently support MySql, SQlite, PostGresSQl,
SQL Server, and sorry for incomplete documentation.

## Usage Instructions

First in you model extends the abstract class *Storage* and implements the getConnector, example:

```php

<?php

namespace Module\Model;

use Storage\Connector\Connector,
    Storage\Connector\ConnectorInterface,
    Storage\Persistence,
    PDO;

abstract class Model extends Persistence {

    /**
     * @return ConnectorInterface
     */
    protected function getConnector(){
        return new Connector(array(
            'dsn' => 'mysql:host=localhost;dbname=social',
            'username' => 'root',
            'password' => 'root',
            'options' => array(
                PDO::ATTR_CASE => PDO::CASE_NATURAL,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
                PDO::ATTR_STRINGIFY_FETCHES => false,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
            )
        ));
    }

}

```

an example of use, remember all function params accepts arrays and infinite clauses, and select accepts multiples tables
in transaction

```php
<?php

namespace Module\Model;

class User extends Model {

    /**
     * @return object
     */
    public function listAllUserWith18Age(){
        return $this->select('User', 'History')->column('name', 'age')->where(['age' => '18'])->orderBy('age')->commit();
    }

    /**
     * @return bool
     */
    public function deleteAllMaleUsers(){
        return $this->delete()->where(['gender' => 'male'])->commit();
    }

    /**
     * @return bool
     */
    public function changeUserAmount(){
        return $this->update()->values(['amount' => 'amount + 1'])->commit();
    }

    /***
     * @return bool|int id of new gender or false case occur error in transaction
     */
    public function insertNewGender(){
        return $this->insert('Gender')->values(['name' => 'undefined'])->commit();
    }
    
}

```