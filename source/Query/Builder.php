<?php

namespace Storage\Query;

use Storage\Connector\ConnectorInterface as Connector;

abstract class Builder {
  private $clause;
  protected $query;
  private $connection;
  protected $values = array();
  private $statements = array();

  public function __construct(Connector $connection, $table){
    $this->flat(func_get_args(), $this->values['table']);
    $this->connection = $connection;
    $this->clause = new Clause();
  }

  private function flat($input = [], &$output = []){
    foreach ($input as $key => $value){
      if (is_array($value)){
        $this->flat(array_filter($value), $output);
      } else if (!empty($value) && !is_null($value)) {
        $output[$key] = $value;
      }
    }
  }

  public function __call($clause, $values){
    $this->flat($values, $this->values[$clause]);
    return $this;
  }

  private function getPatterns(){
    preg_match_all('/{(\w+)}|{(\w+\s+\w+)}/u', $this->query, $matches);
    return array_keys(array_count_values($matches[0]));
  }

  public function commit($realy = true){
    foreach(array_filter($this->values) as $method => $values){
      if (method_exists($this->clause, $method)){
        $this->clause->{$method}(array_filter($values), $this->query, $this->statements);
      } else {
        $this->clause->generic($method, $this->query, $values);
      }
    }
    $this->query = str_replace('{COLUMN}', '*', $this->query);
    $this->query = str_replace($this->getPatterns(), '', $this->query);
    if($realy === false){
      return [$this->query, $this->statements];
    }
    return $this->connection->execute($this->query, array_filter($this->statements));
  }

}
