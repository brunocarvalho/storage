<?php

namespace Storage\Query;

/**
 * @method $this where(mixed ...$values)
 */
class Delete extends Builder {
    protected $query = 'DELETE FROM {TABLE} {WHERE}';
}