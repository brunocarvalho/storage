<?php

namespace Storage\Query;

/**
 * @method $this values(mixed ...$values)
 */
class Insert extends Builder {
    protected $query = 'INSERT INTO {TABLE} {VALUES}';
}