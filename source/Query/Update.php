<?php

namespace Storage\Query;

/**
 * @method $this values(mixed ...$values)
 * @method $this where(mixed ...$where)
 */
class Update extends Builder {
    protected $query = 'UPDATE {TABLE} {VALUES} {WHERE}';
}