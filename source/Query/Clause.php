<?php

namespace Storage\Query;

class Clause {

    /**
     * @return string
     */
    public function randomId(){
        return substr(md5(uniqid(rand(), true)),0,10);
    }

    public function table($table, &$query){
        $tables = array();
        if (count($table) > 0){
            $tables[] = implode(', ', $table);
        }
        $tables = implode(' ', $tables);
        $query = str_replace('{TABLE}', $tables, $query);
    }

    public function join($relation, &$query){
        $query = str_replace('{JOIN}', implode(' ', $relation), $query);
    }

    public function where(array $values, &$query, &$statements){
        $partial = array();
        foreach ($values as $key => $value) {
            if (is_int($key)){
                $partial[] = $value;
            } else {
                $id = $this->randomId();
                $partial[] = "{$key} = :{$id}";
                $statements[$id] = $value;
            }
        }
        $query = str_replace('{WHERE}', 'WHERE ' . implode(' AND ', $partial), $query);
    }

    public function values(array $values, &$query, &$statements){
        $fragment = array();
        foreach($values as $key => $value){
            $id = $this->randomId();
            if (strpos($query, 'INSERT INTO') !== false) {
                $fragment[$key] = ':' .$id;
            } else {
                $fragment[] = "{$key} = :{$id}";
            }
            $statements[$id] = $value;
        }
        if (strpos($query, 'INSERT INTO') !== false){
            $fragment = '(' . implode(', ', array_keys($fragment)) . ') VALUES (' . implode(', ', $fragment) . ')';
        } else {
            $fragment = "SET " . implode(', ', $fragment);
        }
        $query = str_replace('{VALUES}', $fragment, $query);
    }

    public function column(array $values, &$query, &$statements){
        $fragment = array();
        foreach ($values as $key => $value) {
            if (!empty($value)){
                $fragment[] = (!is_int($key)) ? "{$key} AS {$value}" : $value;
            }
        }
        $fragment = (count($fragment) > 0) ? implode(', ', $fragment) : '*';
        $query = str_replace('{COLUMN}', $fragment, $query);
    }

    public function generic($method, &$query, array $values){
        $fragment = array();
        if (count($method) > 0){
            foreach ($values as $columns){
                if (is_array($columns) and count($columns) > 0) {
                    foreach ($columns as $value){
                        $fragment[] = $value;
                    }
                } else if ($columns !== ''){
                    $fragment[] = $columns;
                }
            }
        }
        $method = strtoupper(preg_replace('/([a-z0-9])?([A-Z])/','$1 $2', $method));
        $fragment = implode(' AND ', $fragment);
        $query = str_replace(strtoupper("{{$method}}"), "$method $fragment", $query);
    }

}