<?php

namespace Storage\Query;

/**
 * @method $this where(mixed ...$where)
 * @method $this column(mixed ...$columns)
 * @method $this orderBy(mixed ...$orderBy)
 * @method $this groupBy(mixed ...$groupBy)
 * @method $this having(mixed ...$having)
 * @method $this limit(mixed ...$limit)
 */
class Select extends Builder {
    protected $query = 'SELECT {COLUMN}  FROM {TABLE} {JOIN} {WHERE} {GROUP BY} {HAVING} {ORDER BY} {LIMIT}';

    public function join($table1, $table2, $connector){
        $this->values['join'][] = "INNER JOIN {$table1}.{$connector} = {$table2}.{$connector}";
        return $this;
    }

}