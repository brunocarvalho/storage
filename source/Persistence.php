<?php

namespace Storage;

use ReflectionClass;
use Storage\Query\Delete,
  Storage\Query\Insert,
  Storage\Query\Update,
  Storage\Query\Select,
  Storage\Connector\ConnectorInterface as Connector;

/**
 * Class Persistence
 * @package Storage
 */
abstract class Persistence {
  /**
   * @var array
   */
  private $tables = array();

  /**
   * @return Connector
   */
  protected abstract function getConnector();

  /**
   * set a default table name using class name
   */
  public function __construct(){
    $reflection = new ReflectionClass($this);
    $this->tables[] = $reflection->getShortName();
  }

  /**
   * @param string $table
   * @return Delete
   */
  protected function delete($table = ''){
    $table = (count(func_get_args()) === 0) ? $this->tables : func_get_args();
    return new Delete($this->getConnector(), $table);
  }

  /**
   * @param string $table
   * @return Insert
   */
  protected function insert($table = ''){
    $table = (count(func_get_args()) === 0) ? $this->tables : func_get_args();
    return new Insert($this->getConnector(), $table);
  }

  /**
   * @param string $table
   * @return Update
   */
  protected function update($table = ''){
    $table = (count(func_get_args()) === 0) ? $this->tables : func_get_args();
    return new Update($this->getConnector(), $table);
  }

  /**
   * @param array|string ...$table
   * @return Select
   */
  protected function select(){
    $table = (count(func_get_args()) === 0) ? $this->tables : func_get_args();
    return new Select($this->getConnector(), $table);
  }

  /**
   * count columns length
   *
   * @param array $condition
   * @return int
   */
  protected function count(array $condition = array()){
    $result = $this->select()->column(array("COUNT(*)" => 'total'))->where($condition)->commit();
    return (int) $result[0]->total;
  }

  /**
   * return the last column id
   *
   * @param string $attribute
   * @return int
   */
  protected function last($attribute) {
    $result = $this->select()->column(array('MAX(' . $attribute . ')' => 'last'))->commit();
    return $result[0]->last;
  }

  /**
   * @param string $query
   * @param array $statements
   * @return bool|object
   */
  protected function execute($query, array $statements = array()) {
    return $this->getConnector()->execute($query, $statements);
  }

}
