<?php

namespace Storage\Connector;

use PDO;

class SQlite extends Connector {

    public function __construct($file){
        parent::__construct(array(
            'dsn' => 'sqlite:' . $file,
            'options' => array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
            )
        ));
    }

}