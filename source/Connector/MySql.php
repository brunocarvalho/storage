<?php

namespace Storage\Connector;

use PDO;

class MySql extends Connector {

    public function __construct($host, $database, $user, $password){
        parent::__construct(array(
            'dsn' => 'mysql:host='. $host . ';dbname=' . $database,
            'username' => $user,
            'password' => $password,
            'options' => array(
                PDO::ATTR_CASE => PDO::CASE_NATURAL,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
                PDO::ATTR_STRINGIFY_FETCHES => false,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
            )
        ));
    }

}