<?php

namespace Storage\Connector;

use Exception;
use PDOException,
  PDO;

class Connector implements ConnectorInterface {
  /**
   * @var PDO
   */
  private $connection;

  /**
   * Connection constructor.
   *
   * @param array|PDO $connection
   * @throws Exception
   */
  public function __construct($connection){
    if (!$connection instanceof PDO) {
      if (is_string($connection)) {
        $connection = array('dsn' => $connection);
      }
      if (!is_array($connection)) {
        throw new Exception('First argument to Connection must be an instance of PDO, a DSN string, or a configuration array');
      }
      if (!isset($connection['dsn'])) {
        throw new Exception('configuration array must contain "dsn"');
      }
      // merge optional parameters
      $connection = array_merge(array(
        'username' => null,
        'password' => null,
        'options' => array(),
      ), $connection);
      $connection = new PDO($connection['dsn'], $connection['username'], $connection['password'], $connection['options']);
    }
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($query, array $statements = []){
    $connection = $this->connection->prepare($query);
    $this->connection->beginTransaction();
    try {
      if ($connection->execute($statements)){
        if ($connection->columnCount() !== 0) {
          return $connection->fetchAll();
        } else {
          $return = ($connection->rowCount() > 0) ? true : false;
          if ($return !== false && $this->connection->lastInsertId() !== '0'){
            $return = $this->connection->lastInsertId();
          }
          $this->connection->commit();
          return $return;
        }
      }
    } catch(Exception $error) {
      $this->connection->rollback();
    }
  }

}
