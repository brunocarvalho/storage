<?php

namespace Storage\Connector;

use Exception;

interface ConnectorInterface {

    /**
     * execute an transaction into database
     *
     * @param $query
     * @param array $statements
     * @return object|bool|int|string
     * @throws Exception
     */
    public function execute($query, array $statements = array());

}