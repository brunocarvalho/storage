<?php

namespace Storage\Connector;

use PHPUnit_Framework_TestCase as TestCase;

class SQliteTest extends TestCase {

    public function testConnection(){
        $connection = new SQlite(realpath(__DIR__) . '/storage.sqlite');
        $result = $connection->execute('SELECT 1 + 1 AS result');
        $this->assertEquals(2, $result[0]->result);
    }

}